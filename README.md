# Gemnasium downloads vulnerability database unnecessarily when generating SBOMs

Example project for https://gitlab.com/gitlab-org/gitlab/-/issues/468730

This project contains a simple Java projects using maven.

Gemnasium is invoked using this command: `GEMNASIUM_DB_REMOTE_URL="example.com" /analyzer sbom`

Expected: There should be an SBOM created at `maven/gl-*.cdx.json`.

Actual: Gemnasium fails with this error message:
```
[INFO] Project's dependencies have been succesfully dumped into: /app/maven/gemnasium-maven-plugin.json
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.745 s
[INFO] Finished at: 2024-06-24T16:46:11Z
[INFO] ------------------------------------------------------------------------

[DEBU] [gemnasium-maven] [2024-06-24T16:46:11Z] [/go/src/app/advisory/repo.go:124] ▶ /usr/bin/git -C /gemnasium-db remote set-url origin example.com

[DEBU] [gemnasium-maven] [2024-06-24T16:46:11Z] [/go/src/app/advisory/repo.go:124] ▶ /usr/bin/git -C /gemnasium-db fetch --force --tags origin master
fatal: 'example.com' does not appear to be a git repository
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

See the pipeline for this project.
